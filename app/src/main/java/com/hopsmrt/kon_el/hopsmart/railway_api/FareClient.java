package com.hopsmrt.kon_el.hopsmart.railway_api;



import com.hopsmrt.kon_el.hopsmart.model.fare.FareResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/*
http://api.railwayapi.com/fare/train/12555/source
/gkp/dest/ndls/age/18/quota/PT
/doj/23-11-2014/apikey/myapikey/
 */

public interface FareClient {

    @GET("/fare/train/{train_number}/source" +
            "/{source_code}/dest/{dest_code}/age/" +
            "28/quota/GN/doj/{dd_mm_yyyy}/apikey/brift5318/")
    Call<FareResponse> getFareCall(
            @Path("train_number") String trainNumber,
            @Path("source_code") String sourceCode,
            @Path("dest_code") String destinationCode,
            @Path("dd_mm_yyyy") String date
    );
}
