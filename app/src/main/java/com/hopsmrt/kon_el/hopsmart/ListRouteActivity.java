package com.hopsmrt.kon_el.hopsmart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hopsmrt.kon_el.hopsmart.adapters.HopRouteAdapter;
import com.hopsmrt.kon_el.hopsmart.adapters.SortedHopRouteAdapter;
import com.hopsmrt.kon_el.hopsmart.hopsmart_api.HopsmartServiceGenerator;
import com.hopsmrt.kon_el.hopsmart.hopsmart_api.RouteClient;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.HopRoute;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.HopTrain;
import com.hopsmrt.kon_el.hopsmart.model.routes.RouteBody;
import com.hopsmrt.kon_el.hopsmart.model.routes.H;
import com.hopsmrt.kon_el.hopsmart.model.routes.Route;
import com.hopsmrt.kon_el.hopsmart.model.routes.RoutesObtained;
import com.hopsmrt.kon_el.hopsmart.utils.FormData;
import com.hopsmrt.kon_el.hopsmart.utils.ItemClickListener;
import com.hopsmrt.kon_el.hopsmart.utils.ObjectBusHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ListRouteActivity extends AppCompatActivity {

    private SortedHopRouteAdapter myAdapter;

//    private List<HopRoute> hopRouteList = new ArrayList<>();
//    private HopRouteAdapter myAdapter;

    private List<Route> routes = null;
    private ProgressDialog progressDialog;
    private Button button;
    private boolean loadComplete = false;
    private FormData formData;

    private final static int INCREMENT = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_route);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "WOW u pressed it!! COnGO", Toast.LENGTH_SHORT).show();
                myAdapter.clear();
                myAdapter.invertSortTag();
                populateRouteList(routes);
            }
        });

        formData = ObjectBusHelper.formData;
        RouteBody routeBody = new RouteBody(
                formData.getSource(),
                formData.getDestination(),
                formData.getDateString(),
                formData.getMinBufferTime(),
                formData.getMaxBufferTime(),
                formData.getClassType()
        );


        progressDialog = new ProgressDialog(ListRouteActivity.this);
        progressDialog.setMessage("Loading the awesomeness!!!");
        progressDialog.show();

//        myAdapter = new HopRouteAdapter(hopRouteList);
        myAdapter = new SortedHopRouteAdapter(SortedHopRouteAdapter.SORT_DISTANCE_LOW_TO_HIGH);
        myAdapter.setItemClickListener(new ItemClickListener() {
            @Override
            public void onClick(View view, int position) {

                ObjectBusHelper.hopRoute = myAdapter.getDataAtIndex(position);
                startActivity(new Intent(ListRouteActivity.this, DetailsActivity.class));
            }

            @Override
            public void onLongClick(View view, int position) {
                Toast.makeText(getApplicationContext(), "Coming soon..", Toast.LENGTH_SHORT).show();
            }
        });

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());

        assert recyclerView != null;
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(myAdapter);
        recyclerView.setHasFixedSize(false);
        recyclerView.setNestedScrollingEnabled(false);

        button = (Button) findViewById(R.id.load_more_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                populateRouteList(routes);
            }
        });
        button.setVisibility(View.GONE);

        findRoutes(routeBody);
    }

    private void findRoutes(RouteBody routeBody) {
        RouteClient client = HopsmartServiceGenerator.createService(RouteClient.class);

        Call<RoutesObtained> call = client.routesObtainedCall(routeBody);
        call.enqueue(new Callback<RoutesObtained>() {
            @Override
            public void onResponse(Call<RoutesObtained> call, Response<RoutesObtained> response) {
                routes = response.body().getRoute();
                String toastMsg = "Got " + response.body().getTc() + " routes";
                Toast.makeText(getApplicationContext(), toastMsg, Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
//                hopRouteList.clear();
                populateRouteList(routes);
            }

            @Override
            public void onFailure(Call<RoutesObtained> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "fetching data failed - " + t.toString(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private void populateRouteList(List<Route> routes) {
        if(routes == null) {
            Toast.makeText(getApplicationContext(), "Null routes", Toast.LENGTH_SHORT).show();
            return;
        }

//        int start = hopRouteList.size();
        int start = myAdapter.getItemCount();
        int end = start + INCREMENT;

        for(int i=start; i<end && i<routes.size(); i++) {
            Route route = routes.get(i);
            List<HopTrain> hopTrains = new ArrayList<>();

            for(H h : route.getH())
                hopTrains.add(new HopTrain(h, formData.getClassType()));

            myAdapter.add(new HopRoute(
                    hopTrains, hopTrains.size()-1, 123.1456
            ));

//            hopRouteList.add(new HopRoute(
//                    hopTrains, hopTrains.size()-1, 123.145
//            ));
//            myAdapter.notifyItemInserted(hopRouteList.size()-1);
        }

        if(button.getVisibility() != View.VISIBLE && !loadComplete)
            button.setVisibility(View.VISIBLE);

        if(myAdapter.getItemCount() == routes.size()) {
            loadComplete = true;
            button.setVisibility(View.GONE);
        }

    }
}
