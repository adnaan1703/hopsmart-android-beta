package com.hopsmrt.kon_el.hopsmart.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;

public class AutocompleteButton extends LinearLayout{

    private TextView tvToFrom, tvStationCode, tvStationName;

    public AutocompleteButton(Context context) {
        super(context);
        init(context);
    }

    public AutocompleteButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public AutocompleteButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.autocomplete_button_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        tvToFrom = (TextView) findViewById(R.id.tv_to_from);
        tvStationCode = (TextView) findViewById(R.id.tv_station_code);
        tvStationName = (TextView) findViewById(R.id.tv_station_name);

        tvToFrom.setVisibility(GONE);
        tvStationName.setVisibility(GONE);
        tvStationCode.setVisibility(VISIBLE);
    }

    public void setHint(String hint) {
        tvToFrom.setVisibility(GONE);
        tvStationName.setVisibility(GONE);
        tvStationCode.setVisibility(VISIBLE);
        tvStationCode.setText(hint);
    }

    public void setPlace(String toFrom, String stationName, String stationCode) {
        tvToFrom.setText(toFrom);
        tvStationName.setText(stationName);
        tvStationCode.setText(stationCode);

        tvToFrom.setVisibility(VISIBLE);
        tvStationName.setVisibility(VISIBLE);
        tvStationCode.setVisibility(VISIBLE);
    }

    public String getStationCode() {
        if(tvStationName.getVisibility() == VISIBLE)
            return tvStationCode.getText().toString();
        else
            return null;
    }
}
