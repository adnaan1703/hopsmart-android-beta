package com.hopsmrt.kon_el.hopsmart.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;


public class ClassTypeButton extends LinearLayout {

    TextView tvClassCode, tvClassName;

    public ClassTypeButton(Context context) {
        super(context);
        init(context);
    }

    public ClassTypeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClassTypeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.class_type_button_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvClassCode = (TextView) findViewById(R.id.tv_class_code);
        tvClassName = (TextView) findViewById(R.id.tv_class_name);
    }

    public void setData(String classCode, String className) {
        tvClassCode.setText(classCode);
        tvClassName.setText(className);
    }

    public String getClassCode() {
        return tvClassCode.getText().toString();
    }
}
