package com.hopsmrt.kon_el.hopsmart.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;

import java.util.Locale;

public class CalendarButton extends LinearLayout{

    TextView tvDate, tvMonth, tvDay;

    public CalendarButton(Context context) {
        super(context);
        init(context);
    }

    public CalendarButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CalendarButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.calendar_button_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvDate = (TextView) findViewById(R.id.tv_date);
        tvMonth = (TextView) findViewById(R.id.tv_month);
        tvDay = (TextView) findViewById(R.id.tv_day);
    }

    public void setDate(int date) {
        if(date < 10)
            tvDate.setText(String.format(Locale.UK,"0%d", date));
        else
            tvDate.setText((String.format(Locale.UK,"%d", date)));
    }

    public void setMonth(String month) {
        tvMonth.setText(month);
    }

    public void setDay(String day) {
        tvDay.setText(day);
    }
}
