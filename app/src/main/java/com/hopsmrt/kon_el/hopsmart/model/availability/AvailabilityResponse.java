
package com.hopsmrt.kon_el.hopsmart.model.availability;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class AvailabilityResponse {

    @SerializedName("to")
    @Expose
    private To to;
    @SerializedName("from")
    @Expose
    private From from;
    @SerializedName("last_updated")
    @Expose
    private LastUpdated lastUpdated;
    @SerializedName("class")
    @Expose
    private Class _class;
    @SerializedName("availability")
    @Expose
    private List<Availability> availability = new ArrayList<Availability>();
    @SerializedName("failure_rate")
    @Expose
    private double failureRate;
    @SerializedName("train_number")
    @Expose
    private String trainNumber;
    @SerializedName("quota")
    @Expose
    private Quota quota;
    @SerializedName("train_name")
    @Expose
    private String trainName;
    @SerializedName("response_code")
    @Expose
    private int responseCode;
    @SerializedName("error")
    @Expose
    private String error;

    /**
     * 
     * @return
     *     The to
     */
    public To getTo() {
        return to;
    }

    /**
     * 
     * @param to
     *     The to
     */
    public void setTo(To to) {
        this.to = to;
    }

    /**
     * 
     * @return
     *     The from
     */
    public From getFrom() {
        return from;
    }

    /**
     * 
     * @param from
     *     The from
     */
    public void setFrom(From from) {
        this.from = from;
    }

    /**
     * 
     * @return
     *     The lastUpdated
     */
    public LastUpdated getLastUpdated() {
        return lastUpdated;
    }

    /**
     * 
     * @param lastUpdated
     *     The last_updated
     */
    public void setLastUpdated(LastUpdated lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    /**
     * 
     * @return
     *     The _class
     */
    public Class getClass_() {
        return _class;
    }

    /**
     * 
     * @param _class
     *     The class
     */
    public void setClass_(Class _class) {
        this._class = _class;
    }

    /**
     * 
     * @return
     *     The availability
     */
    public List<Availability> getAvailability() {
        return availability;
    }

    /**
     * 
     * @param availability
     *     The availability
     */
    public void setAvailability(List<Availability> availability) {
        this.availability = availability;
    }

    /**
     * 
     * @return
     *     The failureRate
     */
    public double getFailureRate() {
        return failureRate;
    }

    /**
     * 
     * @param failureRate
     *     The failure_rate
     */
    public void setFailureRate(double failureRate) {
        this.failureRate = failureRate;
    }

    /**
     * 
     * @return
     *     The trainNumber
     */
    public String getTrainNumber() {
        return trainNumber;
    }

    /**
     * 
     * @param trainNumber
     *     The train_number
     */
    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    /**
     * 
     * @return
     *     The quota
     */
    public Quota getQuota() {
        return quota;
    }

    /**
     * 
     * @param quota
     *     The quota
     */
    public void setQuota(Quota quota) {
        this.quota = quota;
    }

    /**
     * 
     * @return
     *     The trainName
     */
    public String getTrainName() {
        return trainName;
    }

    /**
     * 
     * @param trainName
     *     The train_name
     */
    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    /**
     * 
     * @return
     *     The responseCode
     */
    public int getResponseCode() {
        return responseCode;
    }

    /**
     * 
     * @param responseCode
     *     The response_code
     */
    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * 
     * @return
     *     The error
     */
    public String getError() {
        return error;
    }

    /**
     * 
     * @param error
     *     The error
     */
    public void setError(String error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
