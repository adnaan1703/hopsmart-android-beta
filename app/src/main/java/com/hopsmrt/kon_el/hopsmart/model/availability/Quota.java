
package com.hopsmrt.kon_el.hopsmart.model.availability;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class Quota {

    @SerializedName("quota_code")
    @Expose
    private String quotaCode;
    @SerializedName("quota_name")
    @Expose
    private String quotaName;

    /**
     * 
     * @return
     *     The quotaCode
     */
    public String getQuotaCode() {
        return quotaCode;
    }

    /**
     * 
     * @param quotaCode
     *     The quota_code
     */
    public void setQuotaCode(String quotaCode) {
        this.quotaCode = quotaCode;
    }

    /**
     * 
     * @return
     *     The quotaName
     */
    public String getQuotaName() {
        return quotaName;
    }

    /**
     * 
     * @param quotaName
     *     The quota_name
     */
    public void setQuotaName(String quotaName) {
        this.quotaName = quotaName;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
