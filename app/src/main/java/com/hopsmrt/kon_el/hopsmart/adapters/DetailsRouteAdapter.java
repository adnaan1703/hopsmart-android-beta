package com.hopsmrt.kon_el.hopsmart.adapters;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.HopTrain;
import com.hopsmrt.kon_el.hopsmart.model.fare.Fare;
import com.hopsmrt.kon_el.hopsmart.model.fare.FareResponse;
import com.hopsmrt.kon_el.hopsmart.railway_api.FareClient;
import com.hopsmrt.kon_el.hopsmart.railway_api.RailwayServiceGenerator;
import com.hopsmrt.kon_el.hopsmart.utils.StationsFromJson;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DetailsRouteAdapter extends RecyclerView.Adapter<DetailsRouteAdapter.mViewHolder>{

    public interface OnPriceSetListener {
        void onPriceSet(boolean isValid);
    }

    private List<HopTrain> hopTrainList;
    private Context context;

    @Override
    public mViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_details_layout, parent, false);
        return new mViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(mViewHolder holder, int position) {

        StationsFromJson stationsFromJson = new StationsFromJson(context);

        HopTrain hopTrain = hopTrainList.get(position);

        holder.tvSourceTime.setText(hopTrain.getBoardingTime());
        holder.tvDestinationTime.setText(hopTrain.getArrivalTime());
        holder.tvDuration.setText(R.string._03h55m);

        holder.tvSource.setText(stationsFromJson.getStationName(hopTrain.getSource()));
        holder.tvDestination.setText(stationsFromJson.getStationName(hopTrain.getDestination()));
        holder.tvTrain.setText(hopTrain.getTrainName());

        holder.tvAvailability.setText(hopTrain.getAvailability());
        holder.tvPrice.setText(hopTrain.getFare());
        holder.tvDistance.setText(String.format(Locale.UK, "%s Km", hopTrain.getDistance()));
        holder.tvBoardingDate.setText(hopTrain.getBoardingDate());

        holder.tvPrice.setVisibility(View.GONE);
        holder.progressBarPrice.setVisibility(View.VISIBLE);

        getPrice(holder, position);

    }


    @Override
    public int getItemCount() {
        return hopTrainList.size();
    }

    public class mViewHolder extends RecyclerView.ViewHolder {

        TextView tvSourceTime, tvDestinationTime, tvDuration;
        TextView tvSource, tvDestination, tvTrain;
        TextView tvAvailability, tvPrice, tvDistance, tvBoardingDate;

        ProgressBar progressBarPrice;

        public mViewHolder(View view) {
            super(view);

            tvSourceTime = (TextView) view.findViewById(R.id.tv_source_time);
            tvDestinationTime = (TextView) view.findViewById(R.id.tv_destination_time);
            tvDuration = (TextView) view.findViewById(R.id.tv_duration);

            tvSource = (TextView) view.findViewById(R.id.tv_source);
            tvDestination = (TextView) view.findViewById(R.id.tv_destination);
            tvTrain = (TextView) view.findViewById(R.id.tv_train);

            tvAvailability = (TextView) view.findViewById(R.id.tv_availability);
            tvDistance = (TextView) view.findViewById(R.id.tv_distance);
            tvPrice = (TextView) view.findViewById(R.id.tv_price);
            tvBoardingDate = (TextView) view.findViewById(R.id.tv_boarding_date);

            progressBarPrice = (ProgressBar) view.findViewById(R.id.spinner_price);
        }
    }

    public DetailsRouteAdapter(List<HopTrain> hopTrainList, Context context) {
        this.hopTrainList = hopTrainList;
        this.context = context;
    }

    private void getPrice(final mViewHolder holder, final int position) {
        final HopTrain hopTrain = hopTrainList.get(position);
        FareClient client = RailwayServiceGenerator.createService(FareClient.class);
        Call<FareResponse> call = client.getFareCall(
                hopTrain.getTrainNumber(),
                hopTrain.getSource(),
                hopTrain.getDestination(),
                hopTrain.getBoardingDate()
        );

        call.enqueue(new Callback<FareResponse>() {
            @Override
            public void onResponse(Call<FareResponse> call, Response<FareResponse> response) {
                List<Fare> fares = response.body().getFare();
                boolean flag = false;
                for(Fare fare : fares) {
                    if(fare.getCode().trim().equalsIgnoreCase(hopTrain.getClassType())) {
                        setPrice(holder, fare.getFare());
                        hopTrainList.get(position).setFare(fare.getFare());
                        flag = true;
                        break;
                    }
                }

                if(!flag)
                    setPrice(holder, "NA");
            }

            @Override
            public void onFailure(Call<FareResponse> call, Throwable t) {
                setPrice(holder, "NA");
            }
        });

    }

    private void setPrice(mViewHolder holder, String fare) {

        holder.progressBarPrice.setVisibility(View.GONE);
        holder.tvPrice.setVisibility(View.VISIBLE);

        if(fare.equalsIgnoreCase("NA")) {
            holder.tvPrice.setText(fare);
            ((OnPriceSetListener)context).onPriceSet(false);
        } else {
            holder.tvPrice.setText(String.format(Locale.UK, "₹ %s", fare));
            ((OnPriceSetListener)context).onPriceSet(true);
        }

    }
}

// TODO: 24/5/16 implement generic class type for fare computation