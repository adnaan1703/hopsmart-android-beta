
package com.hopsmrt.kon_el.hopsmart.model.routes;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class RoutesObtained {

    @SerializedName("tc")
    @Expose
    private int tc;
    @SerializedName("r")
    @Expose
    private List<Route> route = new ArrayList<Route>();

    /**
     * 
     * @return
     *     The tc
     */
    public int getTc() {
        return tc;
    }

    /**
     * 
     * @param tc
     *     The tc
     */
    public void setTc(int tc) {
        this.tc = tc;
    }

    /**
     * 
     * @return
     *     The r
     */
    public List<Route> getRoute() {
        return route;
    }

    /**
     * 
     * @param route
     *     The r
     */
    public void setRoute(List<Route> route) {
        this.route = route;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
