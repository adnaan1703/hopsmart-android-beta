package com.hopsmrt.kon_el.hopsmart.model.adapter_models;


import java.util.List;

public class HopRoute {
    private List<HopTrain> trains = null;
    private int hopCount;
    private double totalFare;
    private double totalDistance;
    private double bufferTime;

    public HopRoute(List<HopTrain> trains, int hopCount, double bufferTime) {
        this.trains = trains;
        this.hopCount = hopCount;
        this.bufferTime = bufferTime;
        modifyData();
    }

    public List<HopTrain> getTrains() {
        return trains;
    }

    public void setTrains(List<HopTrain> trains) {
        this.trains = trains;
    }

    public int getHopCount() {
        return hopCount;
    }

    public void setHopCount(int hopCount) {
        this.hopCount = hopCount;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public double getBufferTime() {
        return bufferTime;
    }

    public void setBufferTime(double bufferTime) {
        this.bufferTime = bufferTime;
    }


    private void modifyData() {

        boolean flag = false;

        this.totalFare = 0.0;
        this.totalDistance = 0.0;
         for(int i=0; i<trains.size(); i++) {
             trains.get(i).modifyDates();
             totalFare += Double.parseDouble(trains.get(i).getFare());
             totalDistance += Double.parseDouble(trains.get(i).getDistance());
             if(Double.parseDouble(trains.get(i).getFare()) < 0.0)
                 flag = true;
         }

        if(flag)
            this.totalFare = -1.0;
    }

    @Override
    public String toString() {
        String str ="";
        for(HopTrain item : trains)
            str += item.getTrainNumber();

        return str;
    }
}

// TODO: 2. add classCode and quotaCode.
