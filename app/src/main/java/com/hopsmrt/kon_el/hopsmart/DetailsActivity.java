package com.hopsmrt.kon_el.hopsmart;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.adapters.DetailsRouteAdapter;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.HopTrain;
import com.hopsmrt.kon_el.hopsmart.utils.ObjectBusHelper;

import java.util.List;
import java.util.Locale;

public class DetailsActivity extends AppCompatActivity
        implements DetailsRouteAdapter.OnPriceSetListener {

    private int counter = 0;
    private boolean allValid = true;
    private List<HopTrain> hopTrainList = null;
    private TextView tvPrice;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.hide();

        hopTrainList = ObjectBusHelper.hopRoute.getTrains();
        DetailsRouteAdapter mAdapter = new DetailsRouteAdapter(hopTrainList, this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        assert recyclerView != null;
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(false);

        tvPrice = (TextView) findViewById(R.id.tv_total_fare);
        TextView tvDistance = (TextView) findViewById(R.id.tv_total_distance);

        Double totalDistance = 0.0;
        for(HopTrain hopTrain : hopTrainList) {
            totalDistance += Double.parseDouble(hopTrain.getDistance());
        }

        assert tvDistance != null;
        tvDistance.setText(String.format(Locale.UK, "%s Km", totalDistance));

    }

    @Override
    protected void onResume() {
        super.onResume();
        counter = 0;
    }

    @Override
    protected void onStart() {
        super.onStart();
        counter = 0;
    }

    public void goBack(View view) {
        this.onBackPressed();
    }

    @Override
    public void onPriceSet(boolean isValid) {
        counter++;
        if(!isValid)
            allValid = false;

        Double totalFare = 0.0;
        if(counter == hopTrainList.size()) {

            if(!allValid) {
                tvPrice.setText(R.string.na);
                return;
            }

            for(HopTrain hopTrain : hopTrainList)
                totalFare += Double.parseDouble(hopTrain.getFare());

            tvPrice.setText(String.format(Locale.UK, "₹ %s", totalFare));
        }
    }

}

// TODO: 5/6/16 total price is crap improve it dude pls