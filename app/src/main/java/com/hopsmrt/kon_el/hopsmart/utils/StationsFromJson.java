package com.hopsmrt.kon_el.hopsmart.utils;


import android.content.Context;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

public class StationsFromJson {

    private final static String TAG = "StationsFromJson.java";
    private Context context;
    JSONArray stations;

    public StationsFromJson(Context context){
        this.context = context;

        try {
            loadJsonData(readJsonFromAsset("stations.json"));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private void loadJsonData(String jsonString) throws JSONException {
        if(jsonString.isEmpty()) {
            Log.d(TAG, "loadJsonData: empty data");
            return;
        }

        JSONObject jsonObject = new JSONObject(jsonString);
        stations = jsonObject.optJSONArray("stations");
    }

    private String readJsonFromAsset(String fileName) {
        String json;
        try {

            InputStream is = context.getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];

            int dump = is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public String getStationName(String stationCode) {
        for(int i=0; i<stations.length(); i++) {
            if(stations.optJSONObject(i).optString("station_code").equalsIgnoreCase(stationCode))
                return String.format(Locale.UK, "%s (%s)",
                        stations.optJSONObject(i).optString("station_name"), stationCode);
        }
        return "SORRY";
    }
}
