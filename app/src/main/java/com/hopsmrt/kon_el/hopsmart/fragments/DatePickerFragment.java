package com.hopsmrt.kon_el.hopsmart.fragments;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;

import com.hopsmrt.kon_el.hopsmart.utils.MyDate;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

   public interface OnDatePickedListener {
       void onDateSet(int dayOfMonth, String monthOfYear, String dayOfWeek, MyDate date);
   }

    OnDatePickedListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnDatePickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()+
                    "must implement onDatePickedListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_MONTH);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        final String[] days = { "Sunday", "Monday", "Tuesday", "Wednesday",
                                "Thursday", "Friday", "Saturday"
                              };

        final String[] months = {   "January", "February", "March", "April",
                                    "May", "June", "July", "August", "September",
                                    "October", "November", "December"
                                };


        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        MyDate date = new MyDate(dayOfMonth, monthOfYear+1, year);

        mCallback.onDateSet(dayOfMonth, months[monthOfYear], days[calendar.get(Calendar.DAY_OF_WEEK) - 1], date);
    }
}
