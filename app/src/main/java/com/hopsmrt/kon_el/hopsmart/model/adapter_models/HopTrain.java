package com.hopsmrt.kon_el.hopsmart.model.adapter_models;

import com.hopsmrt.kon_el.hopsmart.model.routes.H;

import java.util.Locale;

public class HopTrain {
    private String source;
    private String destination;
    private String trainNumber;
    private String trainName;
    private String distance;
    private String boardingDate;
    private String boardingTime;
    private String arrivalDate;
    private String arrivalTime;
    private String fare;
    private String availability = "NF";
    private String classType;


    public HopTrain(String source, String destination, String trainNumber,
                    String trainName, String distance, String boardingDate,
                    String boardingTime, String arrivalDate, String arrivalTime, String fare) {

        this.source = source;
        this.destination = destination;
        this.trainNumber = trainNumber;
        this.trainName = trainName;
        this.distance = distance;
        this.boardingDate = boardingDate;
        this.boardingTime = boardingTime;
        this.arrivalDate = arrivalDate;
        this.arrivalTime = arrivalTime;
        this.fare = fare;
    }

    public HopTrain(H h, String classType) {
        this.source = h.getS();
        this.destination = h.getD();
        this.trainNumber = h.getTno();
        this.trainName = h.getTn();
        this.distance = h.getDi();
        this.boardingDate = h.getBdt();
        this.boardingTime = h.getBtm();
        this.arrivalDate = h.getAdt();
        this.arrivalTime = h.getAtm();
        this.fare = h.getF();
        this.classType = classType;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getTrainNumber() {
        return trainNumber;
    }

    public void setTrainNumber(String trainNumber) {
        this.trainNumber = trainNumber;
    }

    public String getTrainName() {
        return trainName;
    }

    public void setTrainName(String trainName) {
        this.trainName = trainName;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getBoardingDate() {
        return boardingDate;
    }

    public void setBoardingDate(String boardingDate) {
        this.boardingDate = boardingDate;
    }

    public String getBoardingTime() {
        return boardingTime;
    }

    public void setBoardingTime(String boardingTime) {
        this.boardingTime = boardingTime;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getFare() {
        return fare;
    }

    public void setFare(String fare) {
        this.fare = fare;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

    public void modifyDates() {
        String[] newDate;
        newDate = this.boardingDate.split("-");
        this.boardingDate = String.format(Locale.UK, "%s-%s-%s", newDate[2], newDate[1], newDate[0]);
        newDate = this.arrivalDate.split("-");
        this.arrivalDate = String.format(Locale.UK, "%s-%s-%s", newDate[2], newDate[1], newDate[0]);
    }
}
