package com.hopsmrt.kon_el.hopsmart.utils;

import java.util.Locale;

public class MyDate {
    int day;
    int month;
    int year;

    public MyDate(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }

    public MyDate() {
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    @Override
    public String toString() {
        String str;
        if(day < 10)
            str = String.format(Locale.UK, "0%d-", day);
        else
            str = String.format(Locale.UK, "%d-", day);

        if(month < 10)
            str += String.format(Locale.UK, "0%d-", month);
        else
            str += String.format(Locale.UK, "%d-", month);

        str += String.format(Locale.UK, "%d", year);
        return str;
    }
}
