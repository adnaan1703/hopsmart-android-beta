package com.hopsmrt.kon_el.hopsmart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.StationSuggestion;

import java.util.List;

public class StationsListAdapter extends ArrayAdapter<StationSuggestion>{

    public StationsListAdapter(Context context, List<StationSuggestion> stationSuggestions) {
        super(context, R.layout.row_list_places, stationSuggestions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = LayoutInflater.from(getContext())
                .inflate(R.layout.row_list_places, parent, false);

        StationSuggestion item = getItem(position);

        TextView tvStationName = (TextView) view.findViewById(R.id.tv_station_name);
        TextView tvStationCode = (TextView) view.findViewById(R.id.tv_station_code);

        tvStationName.setText(item.getStationName());
        tvStationCode.setText(item.getStationCode());

        return view;
    }
}
