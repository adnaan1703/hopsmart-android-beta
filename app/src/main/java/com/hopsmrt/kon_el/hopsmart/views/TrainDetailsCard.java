package com.hopsmrt.kon_el.hopsmart.views;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;

public class TrainDetailsCard extends CardView {

    TextView tvSourceTime, tvDestinationTime, tvDuration;
    TextView tvSource, tvDestination, tvTrain;
    TextView tvAvailability, tvPrice, tvDistance;


    public TrainDetailsCard(Context context) {
        super(context);
        init(context);
    }

    public TrainDetailsCard(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TrainDetailsCard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.row_details_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        tvSourceTime = (TextView) findViewById(R.id.tv_source_time);
        tvDestinationTime = (TextView) findViewById(R.id.tv_destination_time);
        tvDuration = (TextView) findViewById(R.id.tv_duration);

        tvSource = (TextView) findViewById(R.id.tv_source);
        tvDestination = (TextView) findViewById(R.id.tv_destination);
        tvTrain = (TextView) findViewById(R.id.tv_train);

        tvAvailability = (TextView) findViewById(R.id.tv_availability);
        tvDistance = (TextView) findViewById(R.id.tv_distance);
        tvPrice = (TextView) findViewById(R.id.tv_price);

    }

    public void setData(String sourceTime, String destinationTime, String duration,
                        String source, String destination, String train,
                        String availability, String distance, String price) {

        tvSourceTime.setText(sourceTime);
        tvDestinationTime.setText(destinationTime);
        tvDuration.setText(duration);

        tvSource.setText(source);
        tvDestination.setText(destination);
        tvTrain.setText(train);

        tvAvailability.setText(availability);
        tvDistance.setText(distance);
        tvPrice.setText(price);
    }
}
