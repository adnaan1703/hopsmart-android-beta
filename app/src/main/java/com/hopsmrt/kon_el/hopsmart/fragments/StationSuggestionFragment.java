package com.hopsmrt.kon_el.hopsmart.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.hopsmrt.kon_el.hopsmart.R;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.StationSuggestion;
import com.hopsmrt.kon_el.hopsmart.utils.Constants;
import com.hopsmrt.kon_el.hopsmart.adapters.StationsListAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class StationSuggestionFragment extends Fragment {

    public interface OnPlaceSelectedListener {
        void onPlaceSelected(String stationName, String stationCode, int placeSelector);
    }

    public final static int SUGGEST_SOURCE = 0;
    public final static int SUGGEST_DESTINATION = 1;
    public final static int SUGGEST_DEFAULT = 2;

    private ListView listView;
    private TextInputLayout inputLayout;
    private List<StationSuggestion> suggestions = new ArrayList<>();
    private List<StationSuggestion> dataList = new ArrayList<>();
    private StationsListAdapter mAdapter = null;
    private OnPlaceSelectedListener mCallback;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String jsonString = readJsonFromAsset("stations.json");

        try {
            loadJsonData(jsonString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void loadJsonData(String jsonString) throws JSONException {

        if(jsonString.isEmpty()) {
            Toast.makeText(getContext(), "empty json string", Toast.LENGTH_SHORT).show();
            return;
        }

        JSONObject jsonObject = new JSONObject(jsonString);
        JSONArray stations = jsonObject.optJSONArray("stations");

        for(int i=0; i< stations.length(); i++) {
            JSONObject item = stations.optJSONObject(i);
            dataList.add(new StationSuggestion(
                    item.optString("station_name"),
                    item.optString("station_code"),
                    item.optString("location")
            ));
        }
    }

    private String readJsonFromAsset(String fileName) {
        String json;
        try {

            InputStream is = getActivity().getAssets().open(fileName);

            int size = is.available();
            byte[] buffer = new byte[size];

            int dump = is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mCallback = (OnPlaceSelectedListener) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement OnPlaceSelectedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.autocomplete_stations_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        inputLayout = (TextInputLayout) getActivity().findViewById(R.id.text_input_layout);
        EditText editText = (EditText) getActivity().findViewById(R.id.et_enter);
        editText.requestFocus();
        listView = (ListView) getActivity().findViewById(R.id.list_stations);

        final int placeSelector = getArguments().getInt(Constants.FRAGMENT_PLACE, SUGGEST_DEFAULT);

        if(placeSelector == SUGGEST_DESTINATION)
            inputLayout.setHint("Enter The Destination");
        else if(placeSelector == SUGGEST_SOURCE)
            inputLayout.setHint("Enter The Source");
        else
            inputLayout.setHint("Enter The Place");


        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                suggestions.clear();
                for(StationSuggestion stationSuggestion : dataList) {
                    if(stationSuggestion.getSearchString().toUpperCase().contains(s.toString().trim().toUpperCase())
                            && s.toString().trim().length() != 0) {
                        suggestions.add(stationSuggestion);
                    }
                }
                mAdapter = new StationsListAdapter(getContext(), suggestions);
                listView.setAdapter(mAdapter);
                if(suggestions.size() == 0)
                    showError();
                else
                    hideError();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StationSuggestion item = suggestions.get(position);
                mCallback.onPlaceSelected(item.getStationName(), item.getStationCode(), placeSelector);
                getActivity().getSupportFragmentManager()
                        .beginTransaction()
                        .remove(StationSuggestionFragment.this)
                        .commit();
            }
        });

        InputMethodManager inputMethodManager = (InputMethodManager)
                getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.showSoftInput(getView(), InputMethodManager.SHOW_IMPLICIT);

    }

    private void showError() {
        inputLayout.setError("No matching destination");
    }

    private void hideError() {
        inputLayout.setError("");
    }
}
