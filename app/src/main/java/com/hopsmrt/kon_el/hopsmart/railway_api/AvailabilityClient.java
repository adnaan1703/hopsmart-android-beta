package com.hopsmrt.kon_el.hopsmart.railway_api;

import com.hopsmrt.kon_el.hopsmart.model.availability.AvailabilityResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface AvailabilityClient {

    @GET("/check_seat/train/{train_number}/source/{source_code}/dest/{dest_code}" +
            "/date/{dd_mm_yyyy}/class/{class_code}/quota/{quota_code}/apikey/brift5318")
    Call<AvailabilityResponse> getAvailabilityCall (
            @Path("train_number") String trainNumber,
            @Path("source_code") String sourceCode,
            @Path("dest_code") String destinationCode,
            @Path("dd_mm_yyyy") String date,
            @Path("class_code") String classCode,
            @Path("quota_code") String quotaCode
    );
}
