package com.hopsmrt.kon_el.hopsmart.utils;

import android.view.View;

public interface ItemClickListener {
    void onClick(View view, int position);
    void onLongClick(View view, int position);
}
