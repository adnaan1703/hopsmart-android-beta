package com.hopsmrt.kon_el.hopsmart;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.hopsmrt.kon_el.hopsmart.fragments.ClassTypePickerFragment;
import com.hopsmrt.kon_el.hopsmart.fragments.StationSuggestionFragment;
import com.hopsmrt.kon_el.hopsmart.utils.Constants;
import com.hopsmrt.kon_el.hopsmart.utils.FormData;
import com.hopsmrt.kon_el.hopsmart.utils.MyDate;
import com.hopsmrt.kon_el.hopsmart.utils.ObjectBusHelper;
import com.hopsmrt.kon_el.hopsmart.views.AutocompleteButton;
import com.hopsmrt.kon_el.hopsmart.views.CalendarButton;
import com.hopsmrt.kon_el.hopsmart.fragments.DatePickerFragment;
import com.hopsmrt.kon_el.hopsmart.views.ClassTypeButton;

import java.util.Locale;

public class MainActivity extends AppCompatActivity
        implements  NavigationView.OnNavigationItemSelectedListener,
        DatePickerFragment.OnDatePickedListener,
        ClassTypePickerFragment.OnClassPickedListener,
        StationSuggestionFragment.OnPlaceSelectedListener {

    private CalendarButton calendarButton;
    private ClassTypeButton classTypeButton;
    private MyDate date = null;

    private AutocompleteButton ab_source, ab_destination;

    private LinearLayout llAdvancedSettings;
    private Switch aSwitch;
    private SeekBar seekMinDay, seekMaxDay, seekMinHr, seekMaxHr;
    private TextView tvMinDay, tvMaxDay, tvMinHr, tvMaxHr;

    private static final int SEEK_BAR_DAY = 0;
    private static final int SEEK_BAR_HOUR = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        calendarButton = (CalendarButton) findViewById(R.id.calendar_button);
        classTypeButton = (ClassTypeButton) findViewById(R.id.class_type_button);
        ab_source = (AutocompleteButton) findViewById(R.id.autocomplete_source);
        ab_destination = (AutocompleteButton) findViewById(R.id.autocomplete_destination);
        llAdvancedSettings = (LinearLayout) findViewById(R.id.ll_advanced_setting);
        aSwitch = (Switch) findViewById(R.id.switch1);

        seekMaxDay = (SeekBar) findViewById(R.id.seekbar_max_day);
        seekMinDay = (SeekBar) findViewById(R.id.seekbar_min_day);
        seekMaxHr = (SeekBar) findViewById(R.id.seekbar_max_hr);
        seekMinHr = (SeekBar) findViewById(R.id.seekbar_min_hr);

        tvMaxDay = (TextView) findViewById(R.id.tv_max_buff_day);
        tvMinDay = (TextView) findViewById(R.id.tv_min_buff_day);
        tvMaxHr = (TextView) findViewById(R.id.tv_max_buff_hr);
        tvMinHr = (TextView) findViewById(R.id.tv_min_buff_hr);

        setViewActions();
        setDefaults();
    }

    private void setViewActions() {

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                    llAdvancedSettings.setVisibility(View.VISIBLE);
                else
                    llAdvancedSettings.setVisibility(View.GONE);
            }
        });

        SeekBar.OnSeekBarChangeListener progressChange = new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                int id = seekBar.getId();
                switch (id) {
                    case R.id.seekbar_min_day:
                        tvMinDay.setText(String.format(Locale.UK, "%d days", progress));
                        break;
                    case R.id.seekbar_min_hr:
                        tvMinHr.setText(String.format(Locale.UK, "%d hrs", progress));
                        break;
                    case R.id.seekbar_max_day:
                        tvMaxDay.setText(String.format(Locale.UK, "%d days", progress));
                        break;
                    case R.id.seekbar_max_hr:
                        tvMaxHr.setText(String.format(Locale.UK, "%d hrs", progress));
                        break;
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        };

        seekMinDay.setOnSeekBarChangeListener(progressChange);
        seekMinHr.setOnSeekBarChangeListener(progressChange);
        seekMaxDay.setOnSeekBarChangeListener(progressChange);
        seekMaxHr.setOnSeekBarChangeListener(progressChange);
    }

    private void setDefaults() {

        ab_source.setHint("Enter The Source");
        ab_destination.setHint("Enter The Destination");
        classTypeButton.setData("SL", "Sleeper Class");
        llAdvancedSettings.setVisibility(View.GONE);
        aSwitch.setChecked(false);

        setSeekBarValue(seekMinDay, tvMinDay, 0, SEEK_BAR_DAY);
        setSeekBarValue(seekMinHr, tvMinHr, 3, SEEK_BAR_HOUR);
        setSeekBarValue(seekMaxDay, tvMaxDay, 0, SEEK_BAR_DAY);
        setSeekBarValue(seekMaxHr, tvMaxHr, 15, SEEK_BAR_HOUR);
    }

    private void setSeekBarValue(SeekBar seekBar, TextView textView, int progress, int selector) {
        seekBar.setProgress(progress);

        if(selector == SEEK_BAR_DAY)
            textView.setText(String.format(Locale.UK, "%d days", progress));
        else
            textView.setText(String.format(Locale.UK, "%d hrs", progress));
    }

    private int getMinBuffer() {

        int hour = seekMinHr.getProgress() * 3600;
        int day = seekMinDay.getProgress() * 86400;

        return hour + day;

    }

    private int getMaxBuffer() {
        int hour = seekMaxHr.getProgress() * 3600;
        int day = seekMaxDay.getProgress() * 86400;

        return hour + day;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void calendarButtonClicked(View view) {
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(getFragmentManager(), "date_picker");

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void classTypeButtonClicked(View view) {
        ClassTypePickerFragment classTypePickerFragment = new ClassTypePickerFragment();
        classTypePickerFragment.show(getFragmentManager(), "class_type_picker");
    }

    @Override
    public void onDateSet(int dayOfMonth, String monthOfYear, String dayOfWeek, MyDate date) {
        calendarButton.setDate(dayOfMonth);
        calendarButton.setMonth(monthOfYear);
        calendarButton.setDay(dayOfWeek);

        this.date = date;

    }

    public void onSubmitClicked(View view) {

        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);

        Intent intent = new Intent(this, ListRouteActivity.class);

        ObjectBusHelper.formData = new FormData(
                ab_source.getStationCode().toUpperCase().trim(),
                ab_destination.getStationCode().toUpperCase().trim(),
                date.toString(),
                String.format(Locale.UK, "%d", getMinBuffer()),
                String.format(Locale.UK, "%d", getMaxBuffer()),
                classTypeButton.getClassCode().toUpperCase().trim()
        );

        if(validFormData())
            startActivity(intent);

    }

    private boolean validFormData() {

        if( ab_source.getStationCode().toUpperCase().trim().isEmpty() ||
                ab_destination.getStationCode().toUpperCase().trim().isEmpty() ||
                date == null ||
                getMinBuffer() > getMaxBuffer() ||
                classTypeButton.getClassCode().toUpperCase().trim().isEmpty()) {

            Toast.makeText(getApplicationContext(), "Invalid Entry", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    @Override
    public void onPlaceSelected(String stationName, String stationCode, int placeSelector) {
        if(placeSelector == StationSuggestionFragment.SUGGEST_SOURCE) {
            ab_source.setPlace("From", stationName, stationCode);
        }
        else if(placeSelector == StationSuggestionFragment.SUGGEST_DESTINATION) {
            ab_destination.setPlace("To", stationName, stationCode);
        }
    }

    public void onSourceClicked(View view) {

        StationSuggestionFragment fragment = new StationSuggestionFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.FRAGMENT_PLACE, StationSuggestionFragment.SUGGEST_SOURCE);
        fragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.drawer_layout, fragment, "SOURCE");
        transaction.commit();
    }

    public void onDestinationClicked(View view) {

        StationSuggestionFragment fragment = new StationSuggestionFragment();
        Bundle args = new Bundle();
        args.putInt(Constants.FRAGMENT_PLACE, StationSuggestionFragment.SUGGEST_DESTINATION);
        fragment.setArguments(args);

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.drawer_layout, fragment, "DESTINATION");
        transaction.commit();
    }

    @Override
    public void onClassPicked(String className, String classCode) {
        classTypeButton.setData(classCode, className);
    }
}

// TODO: 23/5/16 add default values in calendar button
// TODO: 23/5/16 check validation of values in the form
// TODO: 24/5/16 VALIDATION : make sure max buffer time is less than min buffer time
// TODO: 24/5/16 VALIDATION : make sure date selected is future date
