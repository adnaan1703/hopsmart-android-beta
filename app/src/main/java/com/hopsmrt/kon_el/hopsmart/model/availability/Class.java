
package com.hopsmrt.kon_el.hopsmart.model.availability;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class Class {

    @SerializedName("class_code")
    @Expose
    private String classCode;
    @SerializedName("class_name")
    @Expose
    private String className;

    /**
     * 
     * @return
     *     The classCode
     */
    public String getClassCode() {
        return classCode;
    }

    /**
     * 
     * @param classCode
     *     The class_code
     */
    public void setClassCode(String classCode) {
        this.classCode = classCode;
    }

    /**
     * 
     * @return
     *     The className
     */
    public String getClassName() {
        return className;
    }

    /**
     * 
     * @param className
     *     The class_name
     */
    public void setClassName(String className) {
        this.className = className;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
