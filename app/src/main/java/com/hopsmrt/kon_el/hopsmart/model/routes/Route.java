
package com.hopsmrt.kon_el.hopsmart.model.routes;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class Route {

    @SerializedName("bt")
    @Expose
    private String bt;
    @SerializedName("h")
    @Expose
    private List<H> h = new ArrayList<H>();

    /**
     * 
     * @return
     *     The bt
     */
    public String getBt() {
        return bt;
    }

    /**
     * 
     * @param bt
     *     The bt
     */
    public void setBt(String bt) {
        this.bt = bt;
    }

    /**
     * 
     * @return
     *     The h
     */
    public List<H> getH() {
        return h;
    }

    /**
     * 
     * @param h
     *     The h
     */
    public void setH(List<H> h) {
        this.h = h;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
