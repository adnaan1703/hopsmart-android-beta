
package com.hopsmrt.kon_el.hopsmart.model.routes;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class RouteBody {

    @SerializedName("starting_station")
    @Expose
    private String startingStation;
    @SerializedName("destination_station")
    @Expose
    private String destinationStation;
    @SerializedName("boarding_date")
    @Expose
    private String boardingDate;
    @SerializedName("min_buffer_time")
    @Expose
    private String minBufferTime;
    @SerializedName("max_buffer_time")
    @Expose
    private String maxBufferTime;
    @SerializedName("class_type")
    @Expose
    private String classType;

    public RouteBody(String startingStation, String destinationStation, String boardingDate,
                     String minBufferTime, String maxBufferTime, String classType) {
        this.startingStation = startingStation;
        this.destinationStation = destinationStation;
        this.boardingDate = boardingDate;
        this.minBufferTime = minBufferTime;
        this.maxBufferTime = maxBufferTime;
        this.classType = classType;
    }

    /**
     * 
     * @return
     *     The startingStation
     */
    public String getStartingStation() {
        return startingStation;
    }

    /**
     * 
     * @param startingStation
     *     The starting_station
     */
    public void setStartingStation(String startingStation) {
        this.startingStation = startingStation;
    }

    /**
     * 
     * @return
     *     The destinationStation
     */
    public String getDestinationStation() {
        return destinationStation;
    }

    /**
     * 
     * @param destinationStation
     *     The destination_station
     */
    public void setDestinationStation(String destinationStation) {
        this.destinationStation = destinationStation;
    }

    /**
     * 
     * @return
     *     The boardingDate
     */
    public String getBoardingDate() {
        return boardingDate;
    }

    /**
     * 
     * @param boardingDate
     *     The boarding_date
     */
    public void setBoardingDate(String boardingDate) {
        this.boardingDate = boardingDate;
    }

    /**
     * 
     * @return
     *     The minBufferTime
     */
    public String getMinBufferTime() {
        return minBufferTime;
    }

    /**
     * 
     * @param minBufferTime
     *     The min_buffer_time
     */
    public void setMinBufferTime(String minBufferTime) {
        this.minBufferTime = minBufferTime;
    }

    /**
     * 
     * @return
     *     The maxBufferTime
     */
    public String getMaxBufferTime() {
        return maxBufferTime;
    }

    /**
     * 
     * @param maxBufferTime
     *     The max_buffer_time
     */
    public void setMaxBufferTime(String maxBufferTime) {
        this.maxBufferTime = maxBufferTime;
    }

    /**
     * 
     * @return
     *     The classType
     */
    public String getClassType() {
        return classType;
    }

    /**
     * 
     * @param classType
     *     The class_type
     */
    public void setClassType(String classType) {
        this.classType = classType;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
