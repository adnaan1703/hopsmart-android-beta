package com.hopsmrt.kon_el.hopsmart.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;

public class TravellerButton extends LinearLayout {

    TextView tvCount, tvLabel;
    ImageView imageView;

    public TravellerButton(Context context) {
        super(context);
        init(context);
    }

    public TravellerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public TravellerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    protected void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.traveller_button_layout, this);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        tvCount = (TextView) findViewById(R.id.traveller_button_count);
        tvLabel = (TextView) findViewById(R.id.traveller_button_label);
        imageView = (ImageView) findViewById(R.id.traveller_button_image);
    }

    public void setCount(int count) {
        tvCount.setText((String.format("%d", count)));

        if(count > 1) {
            imageView.setImageResource(R.drawable.ic_people);
            tvLabel.setText(R.string.tv_travellers);
        }
        else {
            imageView.setImageResource(R.drawable.ic_person);
            tvLabel.setText(R.string.tv_traveller);
        }
    }
}
