
package com.hopsmrt.kon_el.hopsmart.model.routes;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class H {

    @SerializedName("s")
    @Expose
    private String s;
    @SerializedName("d")
    @Expose
    private String d;
    @SerializedName("tno")
    @Expose
    private String tno;
    @SerializedName("tn")
    @Expose
    private String tn;
    @SerializedName("di")
    @Expose
    private String di;
    @SerializedName("bdt")
    @Expose
    private String bdt;
    @SerializedName("btm")
    @Expose
    private String btm;
    @SerializedName("adt")
    @Expose
    private String adt;
    @SerializedName("atm")
    @Expose
    private String atm;
    @SerializedName("f")
    @Expose
    private String f;

    /**
     * 
     * @return
     *     The s
     */
    public String getS() {
        return s;
    }

    /**
     * 
     * @param s
     *     The s
     */
    public void setS(String s) {
        this.s = s;
    }

    /**
     * 
     * @return
     *     The d
     */
    public String getD() {
        return d;
    }

    /**
     * 
     * @param d
     *     The d
     */
    public void setD(String d) {
        this.d = d;
    }

    /**
     * 
     * @return
     *     The tno
     */
    public String getTno() {
        return tno;
    }

    /**
     * 
     * @param tno
     *     The tno
     */
    public void setTno(String tno) {
        this.tno = tno;
    }

    /**
     * 
     * @return
     *     The tn
     */
    public String getTn() {
        return tn;
    }

    /**
     * 
     * @param tn
     *     The tn
     */
    public void setTn(String tn) {
        this.tn = tn;
    }

    /**
     * 
     * @return
     *     The di
     */
    public String getDi() {
        return di;
    }

    /**
     * 
     * @param di
     *     The di
     */
    public void setDi(String di) {
        this.di = di;
    }

    /**
     * 
     * @return
     *     The bdt
     */
    public String getBdt() {
        return bdt;
    }

    /**
     * 
     * @param bdt
     *     The bdt
     */
    public void setBdt(String bdt) {
        this.bdt = bdt;
    }

    /**
     * 
     * @return
     *     The btm
     */
    public String getBtm() {
        return btm;
    }

    /**
     * 
     * @param btm
     *     The btm
     */
    public void setBtm(String btm) {
        this.btm = btm;
    }

    /**
     * 
     * @return
     *     The adt
     */
    public String getAdt() {
        return adt;
    }

    /**
     * 
     * @param adt
     *     The adt
     */
    public void setAdt(String adt) {
        this.adt = adt;
    }

    /**
     * 
     * @return
     *     The atm
     */
    public String getAtm() {
        return atm;
    }

    /**
     * 
     * @param atm
     *     The atm
     */
    public void setAtm(String atm) {
        this.atm = atm;
    }

    /**
     * 
     * @return
     *     The f
     */
    public String getF() {
        return f;
    }

    /**
     * 
     * @param f
     *     The f
     */
    public void setF(String f) {
        this.f = f;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
