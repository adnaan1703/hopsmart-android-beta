package com.hopsmrt.kon_el.hopsmart.fragments;


import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;

import com.hopsmrt.kon_el.hopsmart.R;

public class ClassTypePickerFragment extends DialogFragment {

    public interface OnClassPickedListener {
        void onClassPicked(String className, String classCode);
    }

    private String className, classCode;

    private OnClassPickedListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnClassPickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                    "must implement OnClassPickedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.class_type_dialog_fragment, container);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.radio_group);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                getClassProp(checkedId);
            }
        });

        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        return view;
    }

    public void getClassProp(int checkedId) {

        switch (checkedId) {
            case R.id.rb_1a:
                className = "AC First Class";
                classCode = "1A";
                break;
            case R.id.rb_2a:
                className = "AC 2 Tier";
                classCode = "2A";
                break;
            case R.id.rb_3a:
                className = "AC 3 Tier";
                classCode = "3A";
                break;
            case R.id.rb_sl:
                className = "Sleeper Class";
                classCode = "SL";
                break;
            case R.id.rb_cc:
                className = "AC Chair Car";
                classCode = "CC";
                break;
            case R.id.rb_ec:
                className = "Economy Class";
                classCode = "EC";
                break;
            case R.id.rb_fc:
                className = "First Class";
                classCode = "FC";
                break;
            case R.id.rb_2s:
                className = "Second Sitting";
                classCode = "2S";
        }
        mCallback.onClassPicked(className, classCode);
        ClassTypePickerFragment.this.dismiss();
    }
}
