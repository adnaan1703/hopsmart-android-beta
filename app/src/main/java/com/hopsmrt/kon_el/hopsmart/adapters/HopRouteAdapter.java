package com.hopsmrt.kon_el.hopsmart.adapters;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.hopsmrt.kon_el.hopsmart.R;
import com.hopsmrt.kon_el.hopsmart.model.adapter_models.HopRoute;
import com.hopsmrt.kon_el.hopsmart.model.availability.AvailabilityResponse;
import com.hopsmrt.kon_el.hopsmart.railway_api.AvailabilityClient;
import com.hopsmrt.kon_el.hopsmart.railway_api.RailwayServiceGenerator;
import com.hopsmrt.kon_el.hopsmart.utils.ItemClickListener;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HopRouteAdapter extends RecyclerView.Adapter<HopRouteAdapter.myViewHolder> {

    private List<HopRoute> hopRouteList;
    private ItemClickListener itemClickListener;

    @Override
    public myViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.route_row_layout, parent, false);
        return new myViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(myViewHolder holder, int position) {
        HopRoute hopRoute = hopRouteList.get(position);

        holder.tvTrain1Number.setText(hopRoute.getTrains().get(0).getTrainNumber());
        holder.tvSource.setText(hopRoute.getTrains().get(0).getSource());
        holder.tvDestination.setText(hopRoute.getTrains().get(hopRoute.getHopCount()).getDestination());
        holder.tvPrice.setText(String.format(Locale.UK, "₹ %.2f", hopRoute.getTotalFare()));
        if(hopRoute.getTotalFare() < 0.0)
            holder.tvPrice.setText(R.string.na);

        if(hopRoute.getHopCount() == 1) {
            holder.linearLayout.setVisibility(View.VISIBLE);
            holder.tvTrain2Number.setText(hopRoute.getTrains().get(1).getTrainNumber());
            holder.tvHop.setText(hopRoute.getTrains().get(1).getSource());
        } else  {
            holder.linearLayout.setVisibility(View.GONE);
        }

        holder.tvPrice.setVisibility(View.GONE);
        holder.tvAvailability.setVisibility(View.GONE);
        holder.progressBar.setVisibility(View.VISIBLE);

        getAvailability(holder, position);
    }

    @Override
    public int getItemCount() {
        return hopRouteList.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder
            implements View.OnClickListener, View.OnLongClickListener {

        public TextView tvTrain1Number, tvTrain2Number;
        public TextView tvSource, tvDestination, tvHop;
        public TextView tvPrice, tvAvailability;
        public LinearLayout linearLayout;
        public ProgressBar progressBar;

        public myViewHolder(View view) {
            super(view);
            tvTrain1Number = (TextView) view.findViewById(R.id.card_train1_number);
            tvTrain2Number = (TextView) view.findViewById(R.id.card_train2_number);
            tvSource = (TextView) view.findViewById(R.id.card_train1_station);
            tvHop = (TextView) view.findViewById(R.id.card_train2_station);
            tvDestination = (TextView) view.findViewById(R.id.card_train3_station);
            tvPrice = (TextView) view.findViewById(R.id.card_price);
            tvAvailability = (TextView) view.findViewById(R.id.card_train_availability);
            linearLayout = (LinearLayout) view.findViewById(R.id.card_train2_layout);
            progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition());
        }

        @Override
        public boolean onLongClick(View v) {
            itemClickListener.onLongClick(v, getAdapterPosition());
            return false;
        }
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public HopRouteAdapter(List<HopRoute> hopRouteList) {
        this.hopRouteList = hopRouteList;
    }

    private void getAvailability(final myViewHolder holder, final int position) {
        HopRoute hopRoute = hopRouteList.get(position);
        AvailabilityClient client = RailwayServiceGenerator.createService(AvailabilityClient.class);

        if(hopRoute.getHopCount() == 0) {
            Call<AvailabilityResponse> call = client.getAvailabilityCall(
                    hopRoute.getTrains().get(0).getTrainNumber(),
                    hopRoute.getTrains().get(0).getSource(),
                    hopRoute.getTrains().get(0).getDestination(),
                    hopRoute.getTrains().get(0).getBoardingDate(),
                    "SL",
                    "GN"
            );

            call.enqueue(new Callback<AvailabilityResponse>() {
                @Override
                public void onResponse(Call<AvailabilityResponse> call, Response<AvailabilityResponse> response) {
                    String availability;
                    if(response.body().getAvailability().size() == 0)
                        availability = "BOO BOO";
                    else
                        availability = response.body().getAvailability().get(0).getStatus();

                    hopRouteList.get(position).getTrains().get(0).setAvailability(availability);
                    setAvailability(holder, availability);
                }

                @Override
                public void onFailure(Call<AvailabilityResponse> call, Throwable t) {
                    setAvailability(holder, "SERVER BUSY");
                    hopRouteList.get(position).getTrains().get(0).setAvailability("SERVER BUSY");
                }
            });
        } else {
            Call<AvailabilityResponse> call1 = client.getAvailabilityCall(
                    hopRoute.getTrains().get(0).getTrainNumber(),
                    hopRoute.getTrains().get(0).getSource(),
                    hopRoute.getTrains().get(0).getDestination(),
                    hopRoute.getTrains().get(0).getBoardingDate(),
                    "SL",
                    "GN"
            );

            final Call<AvailabilityResponse> call2 = client.getAvailabilityCall(
                    hopRoute.getTrains().get(1).getTrainNumber(),
                    hopRoute.getTrains().get(1).getSource(),
                    hopRoute.getTrains().get(1).getDestination(),
                    hopRoute.getTrains().get(1).getBoardingDate(),
                    "SL",
                    "GN"
            );

            call1.enqueue(new Callback<AvailabilityResponse>() {
                @Override
                public void onResponse(Call<AvailabilityResponse> call, Response<AvailabilityResponse> response) {
                    final String[] availability = new String[2];
                    if (response.body().getAvailability().size() == 0)
                        availability[0] = "BOO BOO";
                    else
                        availability[0] = response.body().getAvailability().get(0).getStatus();
                    hopRouteList.get(position).getTrains().get(0).setAvailability(availability[0]);


                    call2.enqueue(new Callback<AvailabilityResponse>() {
                        @Override
                        public void onResponse(Call<AvailabilityResponse> call, Response<AvailabilityResponse> response) {
                            if (response.body().getAvailability().size() == 0)
                                availability[1] = "BOO BOO";
                            else
                                availability[1] = response.body().getAvailability().get(0).getStatus();

                            hopRouteList.get(position).getTrains().get(1).setAvailability(availability[1]);
                            setAvailability(holder, availability[0] + " -> " + availability[1]);
                        }

                        @Override
                        public void onFailure(Call<AvailabilityResponse> call, Throwable t) {
                            setAvailability(holder, "SERVER BUSY");
                            hopRouteList.get(position).getTrains().get(1).setAvailability("SERVER BUSY");
                        }
                    });
                }

                @Override
                public void onFailure(Call<AvailabilityResponse> call, Throwable t) {
                    setAvailability(holder, "SERVER BUSY");
                    hopRouteList.get(position).getTrains().get(0).setAvailability("SERVER BUSY");
                }
            });
        }

    }

    private void setAvailability(myViewHolder holder, String availability) {
        holder.tvAvailability.setText(availability.toUpperCase());
        holder.progressBar.setVisibility(View.GONE);
        holder.tvPrice.setVisibility(View.VISIBLE);
        holder.tvAvailability.setVisibility(View.VISIBLE);

    }
}
