package com.hopsmrt.kon_el.hopsmart.model.adapter_models;

public class StationSuggestion {

    private String stationName;
    private String stationCode;
    private String searchString;

    public StationSuggestion(String stationName, String stationCode, String location) {
        this.stationName = stationName;
        this.stationCode = stationCode;
        this.searchString = stationName.toUpperCase()
                + " " + stationCode.toUpperCase()
                + " " + location.toUpperCase();
    }

    public String getStationName() {
        return stationName;
    }

    public void setStationName(String stationName) {
        this.stationName = stationName;
    }

    public String getStationCode() {
        return stationCode;
    }

    public void setStationCode(String stationCode) {
        this.stationCode = stationCode;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString.toUpperCase();
    }
}
