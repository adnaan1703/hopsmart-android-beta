
package com.hopsmrt.kon_el.hopsmart.model.fare;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

@Generated("org.jsonschema2pojo")
public class Fare {

    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("fare")
    @Expose
    private String fare;

    /**
     * 
     * @return
     *     The code
     */
    public String getCode() {
        return code;
    }

    /**
     * 
     * @param code
     *     The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 
     * @return
     *     The name
     */
    public String getName() {
        return name;
    }

    /**
     * 
     * @param name
     *     The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return
     *     The fare
     */
    public String getFare() {
        return fare;
    }

    /**
     * 
     * @param fare
     *     The fare
     */
    public void setFare(String fare) {
        this.fare = fare;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
