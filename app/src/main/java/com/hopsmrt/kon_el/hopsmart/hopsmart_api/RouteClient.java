package com.hopsmrt.kon_el.hopsmart.hopsmart_api;


import com.hopsmrt.kon_el.hopsmart.model.routes.RouteBody;
import com.hopsmrt.kon_el.hopsmart.model.routes.RoutesObtained;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RouteClient {

    @POST("/findRoutes")
    Call<RoutesObtained> routesObtainedCall(@Body RouteBody routeBody);
}
