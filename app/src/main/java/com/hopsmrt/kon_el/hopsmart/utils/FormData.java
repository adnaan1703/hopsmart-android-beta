package com.hopsmrt.kon_el.hopsmart.utils;

public class FormData {

    private String source;
    private String destination;
    private String dateString;
    private String minBufferTime;
    private String maxBufferTime;
    private String classType;

    public FormData(String source, String destination, String dateString,
                    String minBufferTime, String maxBufferTime, String classType) {
        this.source = source;
        this.destination = destination;
        this.dateString = dateString;
        this.minBufferTime = minBufferTime;
        this.maxBufferTime = maxBufferTime;
        this.classType = classType;
    }

    public String getSource() {
        return source;
    }

    public String getDestination() {
        return destination;
    }

    public String getDateString() {
        return dateString;
    }

    public String getMinBufferTime() {
        return minBufferTime;
    }

    public String getMaxBufferTime() {
        return maxBufferTime;
    }

    public String getClassType() {
        return classType;
    }
}
