package com.hopsmrt.kon_el.hopsmart.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hopsmrt.kon_el.hopsmart.R;

import biz.kasual.materialnumberpicker.MaterialNumberPicker;

public class TravellersPickerFragment extends DialogFragment{

    public interface OnNumberPickedListener {
        void onNumberPicked(int number);
    }

    Button button;
    MaterialNumberPicker numberPicker;
    OnNumberPickedListener mCallback;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (OnNumberPickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString() +
                        "must implement OnNumberPickedListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.number_picker_layout, container);
        getDialog().setTitle("Number of Travellers");

        button = (Button) view.findViewById(R.id.number_picker_submit);
        numberPicker = (MaterialNumberPicker) view.findViewById(R.id.number_picker);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubmit();
            }
        });
        return view;
    }

    private void onSubmit() {
        mCallback.onNumberPicked(numberPicker.getValue());
        this.dismiss();
    }
}
